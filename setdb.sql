DROP TABLE subscription CASCADE;
DROP TABLE forum CASCADE;
DROP TABLE users CASCADE;
DROP TYPE genderEnum;

CREATE TYPE genderEnum as ENUM ('male','female');
CREATE TABLE users (
   id SERIAL PRIMARY KEY,
   fname VARCHAR(25),
   lname VARCHAR(25),
   login VARCHAR(25),
   gender genderEnum,
   is_admin INT CHECK (is_admin = 0 OR is_admin = 1),
   pwd VARCHAR(50)
);

CREATE TABLE forum (
   id SERIAL PRIMARY KEY,
   title VARCHAR(100),
   owner INTEGER REFERENCES users(id),
   description VARCHAR(250),
   expiration DATE
);

CREATE TABLE subscription(
    id_forum INT,
    id_user INT,
    PRIMARY KEY (id_forum,id_user),
    FOREIGN KEY (id_forum) REFERENCES forum(id) ON DELETE CASCADE,
    FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE
);


INSERT INTO users(fname,lname,login,gender,is_admin,pwd) VALUES ('Lucas','Dziurzik','lucas@lucas','male',1,'lucas');
INSERT INTO users(fname,lname,login,gender,is_admin,pwd) VALUES ('Augustin','de Laubier','augustin@augustin','male',1,'augustin');
INSERT INTO users(fname,lname,login,gender,is_admin,pwd) VALUES ('Gerard','Menvussa','oyeah@oyeah','female',0,'oyeah');

INSERT INTO forum (title, owner, description, expiration) VALUES ('Réunion 1', 1, 'Réunion de crise', '2021-09-24');
INSERT INTO forum (title, owner, description, expiration) VALUES ('Réunion 2', 2, 'Réunion pour discuter du temps qui passe', '2023-09-24');
INSERT INTO forum (title, owner, description, expiration) VALUES ('Réunion 3', 3, 'Réunion de crise pour de vrai !', '2022-09-24');

INSERT INTO subscription (id_forum, id_user) VALUES (1,2);
INSERT INTO subscription (id_forum, id_user) VALUES (1,3);
INSERT INTO subscription (id_forum, id_user) VALUES (2,1);
