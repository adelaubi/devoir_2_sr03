package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.Forum;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ServletFormSubscription", value = "/ServletFormSubscription")
public class ServletFormSubscription extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            User you = User.FindUserByLogin((String) session.getAttribute("login"));
            List<User> users = User.FindAll();
            List<Forum> yourForums = Forum.FindYourForum(you.getId());
            users.remove(you);
            PrintWriter out = response.getWriter();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Inviter un utilisateur à un chat</title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println(Navbar.getNavbar((String) session.getAttribute("role")));
            out.println("<div class=\"titrePage\">Inviter un utilisateur dans votre chat</div>");
            out.println("<form class=\"formulaire\" action='ServletUpdateSubscription' method='post'>");
            out.println("<table class=\"tableauAuthentification\">");
            out.println("<tr><td><label>Utilisateur à inviter</label></td>");
            out.println("<td><select name='user'>");
            for (User user : users){

                out.println("<option value="+user.getLogin()+">" + user.getLogin() + "</option>");
            }
            out.println("</select>");
            out.println("<select name='forum'>");
            for (Forum forum : yourForums){
                out.println("<option value="+forum.getId()+">" + forum.getTitle() + "</option>");
            }
            out.println("</select></td>");
            out.println("</table>");
            out.println("<input type='hidden' name='action' value='insert'>");
            out.println("<input type='submit' value='Inviter'>");
            out.println("</form>");
            out.println("</body></html>");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
