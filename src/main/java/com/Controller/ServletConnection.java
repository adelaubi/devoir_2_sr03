package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletConnection", urlPatterns = {"/ServletConnection"})
public class ServletConnection extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            // Vérifier si le login existe
            User u = User.FindByloginAndPwd(request.getParameter("username"), request.getParameter("password")); //PB1
            if (u == null) {
                response.setContentType("text/html;charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet Connexion</title>");
                    out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Echec :mot de passe ou login érroné </h1>");
                    out.println("</body>");
                    out.println("</html>");
                }
            } else {
                HttpSession session = request.getSession();
                session.setAttribute("login", u.getLogin());
                String role = u.getRole();
                session.setAttribute("role", role);
                response.setContentType("text/html;charset=UTF-8");
                if ("admin".equalsIgnoreCase(role)) {
                    try (PrintWriter out = response.getWriter()) {
                        out.println("<!DOCTYPE html>");
                        out.println("<html><head><title>Navigation Administrateur</title><link rel=\"stylesheet\" href=\"style.css\">" +
                                "<meta http-equiv=\"refresh\" content=\"0; URL=ServletMenu\"></head>");
                        out.println("<body>");
                        out.println("</body>");
                        out.println("</html>");
                    }

                } else {
                    RequestDispatcher dis = request.getRequestDispatcher("ServletMenu");
                    dis.forward(request,response);
                }

            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @Override
    public void init() throws ServletException {
        super.init();

    }

}
