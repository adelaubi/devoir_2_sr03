package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.Forum;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

@WebServlet(name = "ServletForumManager", value = "/ServletForumManager")
public class ServletForumManager extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try (PrintWriter out = response.getWriter()) {
                HttpSession session = request.getSession();
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Liste des forums :</title>");
                out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                out.println("</head>");
                out.println("<body>");
                out.println(Navbar.getNavbar((String) session.getAttribute("role")));
                out.println("<div class=\"titrePage\">Liste des forums</div>");
                out.println("<table>");
                out.println("<th><td>Nom</td><td>Description</td><td>Date d'expiration</td></th>");
                List<Forum> listForum;
                try {
                    listForum = Forum.FindAll();
                    for (int index = 0; index < listForum.size(); index++) {
                        out.println("<tr>");
                        out.println("<td>");
                        out.println(listForum.get(index).getTitle());
                        out.println("</td>");
                        out.println("<td>");
                        out.println(listForum.get(index).getDescription());
                        out.println("</td>");
                        out.println("<td>");
                        out.println(listForum.get(index).getExpiration());
                        out.println("</td>");
                        out.println("</tr>");
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    System.out.println(ex);
                }
                out.println("</table></body></html>");
            }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //Ajout d'un Forum
            HttpSession session = request.getSession();
            PrintWriter out = response.getWriter();

            String title = request.getParameter("title");
            String description =request.getParameter("description");
            String ownerlogin = (String) session.getAttribute("login");

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            String exp = request.getParameter("expiration");
            LocalDate expiration = LocalDate.parse(exp);

            User owner = User.FindUserByLogin(ownerlogin);
            Forum forum = new Forum(title,description,owner, expiration);
            forum.save();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Forum ajouté </title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println(Navbar.getNavbar((String) session.getAttribute("role")));
            out.println("<h1> Un nouveau forum est ajouté : </h1>");
            out.println(forum.toString());
            out.println("</body>");
            out.println("</html>");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
    }
}
