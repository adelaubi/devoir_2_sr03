package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.MyConnectionClass;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "ServletUpdateSubscription", value = "/ServletUpdateSubscription")
public class ServletUpdateSubscription extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response){ }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user = User.FindUserByLogin(request.getParameter("user"));
            HttpSession session = request.getSession();
            int id_forum = Integer.parseInt(request.getParameter("forum"));
            String action = request.getParameter("action");
            PrintWriter out = response.getWriter();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Mise à jour de vos chats suivis</title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println(Navbar.getNavbar((String) session.getAttribute("role")));
            if (action.equals("insert")){
                if (user.isInForum(id_forum)){
                    out.println("<h1>L'utilisateur est déjà dans votre forum.</h1>");
                    out.println("</body>");
                    out.println("</html>");
                }
                else {
                    user.addForumSubscription(id_forum);
                    out.println("<h1>Un utilisateur a été ajouté à votre chat</h1>");
                    out.println("</body>");
                    out.println("</html>");
                }
            }
            else if (action.equals("delete")) {
                user.deleteForumSubscriptions(id_forum);
                out.println("<h1>Désinscription du chat</h1>");
                out.println("</body>");
                out.println("</html>");
            }
            else{
                out.println("<h1>BUG</h1>");
                out.println("</body>");
                out.println("</html>");
            }

        } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
        }

    }
}

