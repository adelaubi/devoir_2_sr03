package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.Forum;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@WebServlet(name = "ServletMenu", value = "/ServletMenu")
public class ServletMenu extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        List<Forum> forumSubscribed = Objects.requireNonNull(User.FindUserByLogin((String) session.getAttribute("login"))).getForumSubscriptions();
        out.println("<!DOCTYPE html>");
        out.println("<html><head><title>Menu</title><link rel=\"stylesheet\" href=\"style.css\"></head>");
        out.println("<body>");
        out.println(Navbar.getNavbar((String)session.getAttribute("role")));
        out.println("<div class='titrePage'>Vos salons de discussions :</div>");
        out.println("<table><tr><th>Nom</th><th>Description</th><th>Date d'expiration</th><th></th><th></th></tr>");
        List<Forum> listForum;
        try {
            listForum = Forum.FindAll();
            for (Forum forum : listForum) {
                if (forum.getOwner().getLogin().equals(session.getAttribute("login"))) {
                    if(forum.getExpiration().isAfter(LocalDate.now())) {
                        out.println("<tr>");
                        out.println("<td>");
                        out.println(forum.getTitle());
                        out.println("</td>");
                        out.println("<td>");
                        out.println(forum.getDescription());
                        out.println("</td>");
                        out.println("<td>");
                        out.println(forum.getExpiration());
                        out.println("</td>");
                        out.println("<td>");
                        out.println("<form action ='chat.jsp' method ='post'>");
                        out.println("<input type='hidden' name='forumId' value='" + forum.getId() + "'/>");
                        out.println("<input type='submit' name='submitButton' value='Rentrer dans le chat'/>");
                        out.println("</form>");
                        out.println("</td>");
                        out.println("<td>");
                        out.println("<form action ='ServletDeleteForum' method ='post'>");
                        out.println("<input type='hidden' name='forumId' value='" + forum.getId() + "'/>");
                        out.println("<input type='submit' name='submitButton' value='Supprimer le chat'/>");
                        out.println("</form>");
                        out.println("</td>");
                        out.println("<td>");
                        out.println("<form action ='ServletSubscribedManager' method ='post'>");
                        out.println("<input type='hidden' name='forumId' value='" + forum.getId() + "'/>");
                        out.println("<input type='submit' name='submitButton' value='Afficher les utilisateur de ce chat'/>");
                        out.println("</form>");
                        out.println("<a href='./edit_forum.jsp?id=" + forum.getId() + "'><input type='button' name='submitButton' value='Modifier ce chat'/></a>");
                        out.println("</td>");
                        out.println("</tr>");
                    }
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }

        out.println("</table>");
        out.println("<div class='titrePage'>Salons de discussions où vous êtes invité :</div><br/>");
        out.println("<table><tr><th>Nom</th><th>Description</th><th>Date d'expiration</th><th></th><th></th></tr>");
        for (Forum forum : forumSubscribed){
            out.println("<tr>");
            out.println("<td>");
            out.println(forum.getTitle());
            out.println("</td>");
            out.println("<td>");
            out.println(forum.getDescription());
            out.println("</td>");
            out.println("<td>");
            out.println(forum.getExpiration());
            out.println("</td>");
            out.println("<td>");
            out.println("<form action ='chat.jsp' method ='post'>");
            out.println("<input type='hidden' name='forumId' value='"+forum.getId()+"'/>");
            out.println("<input type='submit' name='submitButton' value='Rentrer dans le chat'/>");
            out.println("</form>");
            out.println("</td>");
        }
        out.println("</table>");
        out.println("</body>");
        out.println("</html>");
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }
}
