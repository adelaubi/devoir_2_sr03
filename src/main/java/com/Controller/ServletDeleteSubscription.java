package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.Forum;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ServletDeleteSubscription", value = "/ServletDeleteSubscription")
public class ServletDeleteSubscription extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();

            User you = User.FindUserByLogin((String) session.getAttribute("login"));
            List<Forum> yourForums = you != null ? you.getForumSubscriptions() : null;

            PrintWriter out = response.getWriter();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Se désinscrire d'un chat</title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println(Navbar.getNavbar((String) session.getAttribute("role")));
            out.println("<div class=\"titrePage\">Se désinscrire d'un chat</div>");
            out.println("<form class=\"formulaire\" action='ServletUpdateSubscription' method='post'>");
            out.println("<input type='hidden' name='user' value="+you.getLogin()+">");
            out.println("<table class=\"tableauAuthentification\">");
            out.println("<label>Forum duquel se désinscrire</label>");
            out.println("<select name='forum'>");
            for (Forum forum : yourForums){
                out.println("<option value="+forum.getId()+">" + forum.getTitle() + "</option>");
            }
            out.println("</select>");
            out.println("<input type='hidden' name='action' value='delete'>");
            out.println("<input type='submit' value='Se désinscrire'>");
            out.println("</table>");
            out.println("</form>");
            out.println("</body></html>");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
