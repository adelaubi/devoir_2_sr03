package com.Controller;
import com.HtmlClass.Navbar;
import com.Model.MyConnectionClass;
import com.Model.User;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "ServletDeleteForum", value = "/ServletDeleteForum")
public class ServletDeleteForum extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int forumId = Integer.parseInt(request.getParameter("forumId"));
        HttpSession session = request.getSession();
        try {
            Connection conn = MyConnectionClass.getInstance();
            String delete_query = "DELETE FROM forum WHERE id = " + forumId  ;
            Statement s = null;
            s = conn.createStatement();
            s.executeUpdate(delete_query);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Suppression d'un forum</title><link rel=\"stylesheet\" href=\"style.css\"></head>");
        out.println("<body>");
        out.println(Navbar.getNavbar((String) session.getAttribute("role")));
        out.println("<div class='titrePage'> Le salon a bien été supprimé </div>");
        out.println("<p>Les utilisateurs ne verront plus ce salon désormais.</p>");
    }
}
