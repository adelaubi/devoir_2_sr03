package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.Forum;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

@WebServlet(name = "ServletEditForum", value = "/ServletEditForum")
public class ServletEditForum extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //Modification d'un Forum
            HttpSession session = request.getSession();
            PrintWriter out = response.getWriter();

            String title = request.getParameter("title");
            String description =request.getParameter("description");
            String ownerlogin = (String) session.getAttribute("login");

            String exp = request.getParameter("expiration");
            LocalDate expiration = LocalDate.parse(exp);

            User owner = User.FindUserByLogin(ownerlogin);
            Forum forum = new Forum(Integer.parseInt(request.getParameter("forumId")));
            forum.setDescription(description);
            forum.setTitle(title);
            forum.setExpiration(expiration);
            forum.save();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Forum Modifié </title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println(Navbar.getNavbar((String) session.getAttribute("role")));
            out.println("<h1> Le forum a été modifié : </h1>");
            out.println(forum.toString());
            out.println("</body>");
            out.println("</html>");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
    }
}
