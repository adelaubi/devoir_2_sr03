package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "ServletUserManager", urlPatterns = {"/ServletUserManager"})
public class ServletUserManager extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
    }

    /**
     * Processes requests for both HTTP  <code>POST</code> methods.
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("login") == null || !"admin".equalsIgnoreCase((String) session.getAttribute("role"))) {
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<meta http-equiv='refresh' content='5; URL=connection.jsp' />");
                out.println("<title> Non autorisé</title>");
                out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Vous n'êtes pas connecté ou vous n'êtes pas admin => redirigé vers la page connexion </h1>");
                out.println("</body>");
                out.println("</html>");
            }

        } else {

            try {
                boolean isUnique = true;
                String firstName = request.getParameter("frname");
                String lastName = request.getParameter("faname");
                String mail = request.getParameter("email");
                String gender = request.getParameter("gender");
                String password = request.getParameter("pwd");
                User user = new User(lastName, firstName, mail, gender, password);
                if (request.getParameter("role") != null) {
                    user.setRole(request.getParameter("role"));
                }
                //VERIFICATION DES DOUBLONS
                List<User> listUser;
                listUser = User.FindAll();
                int index = 0;
                while(true){
                    boolean sameFrName = user.getFirstName().equals(listUser.get(index).getFirstName());
                    boolean sameFaName = user.getLastName().equals(listUser.get(index).getLastName());
                    boolean sameLogin = user.getLogin().equals(listUser.get(index).getLogin());
                    boolean sameGender = user.getGender().equals(listUser.get(index).getGender());
                    if (sameFrName && sameFaName && sameLogin && sameGender){
                        isUnique = false;
                        request.setAttribute("user",user);
                        request.getRequestDispatcher("ServletValidation").forward(request,response);
                    }
                    index++;
                    if (index == listUser.size()) break;
                }
                //FIN VERIFICATION DES DOUBLONS
                user.save();
                response.setContentType("text/html;charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println(Navbar.getNavbar((String) session.getAttribute("role")));
                    out.println("<title>Un nouveau utilisateur </title>");
                    out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1> Un nouvel utilisateur est ajouté : </h1>");
                    out.println(user.toString());
                    out.println("</body>");
                    out.println("</html>");
                }
            } catch (SQLException | ClassNotFoundException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("login") == null || "admin".equalsIgnoreCase((String) session.getAttribute("role")) == false) {
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<meta http-equiv='refresh' content='5; URL=connection.jsp' />");
                out.println("<title> Non autorisé</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Vous n'êtes pas connecté ou vous n'êtes pas admin => redirigé vers la page connexion </h1>");
                out.println("</body>");
                out.println("</html>");
            }

        } else {
            try (PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Un nouveau utilisateur </title>");
                out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                out.println("</head>");
                out.println("<body>");
                out.println(Navbar.getNavbar((String) session.getAttribute("role")));
                out.println("<div class='titrePage'> Liste des utilisateurs : </div>");
                out.println("<ul class=\"liste\">");
                List<User> listUser;
                try {
                    listUser = User.FindAll();
                    for (int index = 0; index < listUser.size(); index++) {
                        out.println("<li>");
                        out.println(listUser.get(index).toString());
                        out.println("</li>");
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    System.out.println(ex);
                }

                out.println("</ul>");
                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.

     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
