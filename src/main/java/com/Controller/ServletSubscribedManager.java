package com.Controller;
import com.HtmlClass.Navbar;
import com.Model.Forum;
import com.Model.User;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ServletSubscribedManager", value = "/ServletSubscribedManager")
public class ServletSubscribedManager extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int idForum = Integer.parseInt(request.getParameter("forumId"));
            PrintWriter out = response.getWriter();
            out.println("<html>" +
                    "<head>" +
                    "<title>Utilisateurs du chat</title>" +
                    "<link rel=\"stylesheet\" href=\"style.css\">" +
                    "</head>" +
                    "<body>");
            out.println(Navbar.getNavbar((String) request.getSession().getAttribute("role")));
            out.println("<div class=\"titrePage\">Liste des utilisateurs dans ce chat</div>");
            out.println("<table>");
            out.println("<th><td>Prénom</td><td>Nom</td><td>Login</td></th>");
            List<User> users = User.FindAll();
            for (User user : users) {
                if(user.isInForum(idForum) && user.getId() != Forum.findById(idForum).getOwner().getId()) {
                out.println("<tr>");
                out.println("<td>");
                out.println(user.getFirstName());
                out.println("</td>");
                out.println("<td>");
                out.println(user.getLastName());
                out.println("</td>");
                out.println("<td>");
                out.println(user.getLogin());
                out.println("</td>");
                out.println("<td>");
                out.println("<form action ='ServletUpdateSubscription' method ='post'>");
                out.println("<input type='hidden' name='user' value='" + user.getLogin() + "'/>");
                out.println("<input type='hidden' name='forum' value='" + idForum + "'/>");
                out.println("<input type='hidden' name='action' value='delete'>");
                out.println("<input type='submit' value='Désinscrire cet utilisateur'>");
                out.println("</form>");
                out.println("</td>");
                out.println("</tr>");
                }
            }
            out.println("</table></body></html>");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
