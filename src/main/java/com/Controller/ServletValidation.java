package com.Controller;

import com.HtmlClass.Navbar;
import com.Model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet(name = "ServletValidation", value = "/ServletValidation")
public class ServletValidation extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getAttribute("user");
        String firstName;
        String lastName;
        String email;
        String gender;
        String password;
        if(user == null){
            firstName = request.getParameter("frname");
            lastName = request.getParameter("faname");
            email = request.getParameter("email");
            gender = request.getParameter("gender");
            password = request.getParameter("pwd");
            user = new User(firstName,lastName,email,gender,password);
        }
        else{
            firstName = user.getFirstName();
            lastName = user.getLastName();
            email = user.getLogin();
            gender = user.getGender();
            password = user.getPwd();
        }
        String isValided = (String) request.getParameter("isValided");
        if(isValided == null){
            try (PrintWriter out = response.getWriter()) {
                HttpSession session = request.getSession();
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println(Navbar.getNavbar((String) session.getAttribute("role")));
                out.println("<title>Servlet Validation</title>");
                out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Un utilisateur avec les mêmes nom et prénom existe déjà. Voulez-vous l'enregistrer ?  </h1>");
                out.println("<form method='POST' action='ServletValidation'>");
                out.println("Oui <input type='radio' name='isValided' value='oui' /> ");
                out.println("Non <input type='radio' name='isValided' value='non' />");
                out.println("<input type='hidden' name='frname' value='" + firstName + "'/>");
                out.println("<input type='hidden' name='faname' value='" + lastName + "'/>");
                out.println("<input type='hidden' name='email' value='" + email + "'/>");
                out.println("<input type='hidden' name='gender' value='" + gender + "'/>");
                out.println("<input type='hidden' name='pwd' value='" + password + "' />");
                out.println("<br>");
                out.println("<input type ='submit' value='Envoyer' name='Valider' />");
                out.println("<a href=\"index.jsp\">Retour au menu</a>");
                out.println("</form>");
                out.println("</body>");
                out.println("</html>");
            }
        }else{
            try {
                PrintWriter out = response.getWriter();
                user.save();
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Un nouveau utilisateur </title>");
                out.println("<link rel=\"stylesheet\" href=\"style.css\">");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1> Un nouveau utilisateur est ajouté : </h1>");
                out.println(user.toString());
                out.println("</body>");
                out.println("</html>");
            } catch (SQLException | ClassNotFoundException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
