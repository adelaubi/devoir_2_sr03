package com.HtmlClass;

public class Navbar {
    private static String htmlCode = "<ul class=\"navbar\">\n" +
            "  <li><a href=\"ServletMenu\">Chats</a></li>\n" +
            "  <li><a href=\"add_forum.jsp\">Créer un Salon</a></li>\n" +
            "  <li><a href=\"ServletDeleteSubscription\">Se désinscrire d'un Salon</a></li>\n" +
            "  <li><a href=\"ServletFormSubscription\">Ajouter quelqu'un dans un salon</a></li>\n" +
            "  <li><a href=\"ServletDeconnection\">Se déconnecter</a></li>\n" +
            "</ul>";

    private static String htmlCodeAdmin = "<ul class=\"navbar\">\n" +
            "  <li><a href=\"ServletMenu\">Chats</a></li>\n" +
            "  <li><a href=\"add_forum.jsp\">Créer un Salon</a></li>\n" +
            "  <li><a href=\"ServletDeleteSubscription\">Se désinscrire d'un Salon</a></li>\n" +
            "  <li><a href=\"ServletFormSubscription\">Ajouter quelqu'un </a></li>\n" +
            "  <li><a href=\"add_user.jsp\">Ajouter un nouveau membre</a></li>\n" +
            "  <li><a href=\"ServletUserManager\">Afficher les utilisateurs </a></li>\n" +
            "  <li><a href=\"ServletForumManager\">Afficher les Salons </a></li>\n" +
            "  <li><a href=\"ServletDeconnection\">Se déconnecter</a></li>\n" +
            "</ul>";

    public static String getNavbar(String role) {
        return role.equals("Admin") ?htmlCodeAdmin : htmlCode;
    }

}
