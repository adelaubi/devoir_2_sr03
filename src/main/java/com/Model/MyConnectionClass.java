package com.Model;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MyConnectionClass {
    private static Connection singleton;

    private MyConnectionClass() throws IOException, ClassNotFoundException, SQLException {
        Properties param = new Properties();
        FileInputStream ip= new FileInputStream("/Users/augustindelaubier/sr03_td/devoir_2_SR03/src/main/resources/fileProps.properties");
        param.load(ip);
        String driver = "org.postgresql.Driver";
        String url = param.getProperty("url");
        String utilisateur = param.getProperty("utilisateur");
        String mdp = param.getProperty("mdp");
        Class.forName(driver);
        singleton = DriverManager.getConnection(url, utilisateur, mdp);
    }

    public static Connection getInstance() throws IOException, ClassNotFoundException, SQLException {
        if (singleton == null) {
            new MyConnectionClass();
        }
        return singleton;
    }

    public static void main(String [] args) {
        try {
            Connection con = MyConnectionClass.getInstance();
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

}
