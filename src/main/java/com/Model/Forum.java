package com.Model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.time.LocalDate;


public class Forum extends ActiveRecordBase {

    private String title;
    private String description;
    private User owner;
    private LocalDate expiration;
    private static String _query = "SELECT * FROM forum";

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
    public Forum(ResultSet res) throws SQLException, IOException, ClassNotFoundException {
        this.id = res.getInt("id");
        this.title = res.getString("title");
        this.owner = User.FindByID(res.getInt("owner"));
        this.description = res.getString("description");
        this.expiration = LocalDate.parse(res.getString("expiration"));
        _builtFromDB = true;
    }

    public Forum(String titre, String description, User u, LocalDate expiration) {
        this.title = titre;
        this.description = description;
        this.owner = u;
        this.expiration = expiration;
        _builtFromDB = false;
    }

    public Forum() {
        _builtFromDB = false;
    }

    public Forum(int id) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "select * from forum where id = " + id + ";";
        Statement sql = null;
        sql = conn.createStatement();
        ResultSet res = sql.executeQuery(select_query);
        if (res.next()) {
            this.id = res.getInt("id");
            this.title = res.getString("title");
            this.owner = new User(res.getInt("owner"));
            this.description = res.getString("description");
            this.expiration = LocalDate.parse(res.getString("expiration"));
            _builtFromDB = true;
        }
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public LocalDate getExpiration() {return expiration; }
    public String getExpirationToString() {return String.format("%04d", expiration.getYear()) + "-" + String.format("%02d", expiration.getMonthValue()) + "-" + String.format("%02d", expiration.getDayOfMonth());}

    public void setExpiration(LocalDate expiration) {
        this.expiration = expiration;
    }

    // DB access method
    @Override
    protected String _delete() {
        return "DELETE FROM forum WHERE (id = " + id + ");";
    }

    @Override
    protected String _insert() {
        return "INSERT INTO forum (title, owner, description, expiration) "
                + "VALUES ('" + title + "', " + owner.getId() + ",'"+ description +"','" + expiration + "');";
    }

    @Override
    protected String _update() {
        return "UPDATE forum SET title = '" + title + "', "
                + "owner='" + owner.getId() + "', description = '"+description+"', expiration = '"+expiration+"'   WHERE (id = " + id + ");";
    }


    public static List<Forum> FindAll() throws SQLException, IOException, ClassNotFoundException {
        List <Forum>  listForum = new ArrayList<Forum>() ;

        Connection conn = MyConnectionClass.getInstance();
        Statement sql = conn.createStatement();
        ResultSet res = sql.executeQuery(_query);
        while (res.next()) {
            Forum newForum= new Forum(res);
            listForum.add(newForum);
        }
        return listForum;
    }

    public static Forum findById(int id) throws SQLException, IOException, ClassNotFoundException {
        String select_query = "SELECT * FROM forum WHERE id = " + id;

        Connection conn = MyConnectionClass.getInstance();
        Statement sql = conn.createStatement();
        ResultSet res = sql.executeQuery(select_query);
        if (res.next()) {
            return new Forum(res);
        }
        return null;
    }

    @Override
    public String toString(){
        return "Forum :{ titre : "+ this.title +"; propriétaire : "+this.owner.getLogin()+"; description : "+this.description+"}";
    }

    public  static List<Forum> FindYourForum(int id_user) throws SQLException, IOException, ClassNotFoundException {
        List<Forum> yourForum = new ArrayList<Forum>();

        String select_query = "SELECT * FROM forum WHERE owner = " + id_user;
        Connection conn = MyConnectionClass.getInstance();
        Statement sql = conn.createStatement();
        ResultSet res = sql.executeQuery(select_query);
        while(res.next()) {
            Forum newForum = new Forum(res);
            yourForum.add(newForum);
        }
        return yourForum;
    }
}
