package com.Model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class ActiveRecordBase {
    protected int id;
    protected boolean _builtFromDB;

    public int getId(){return id;}
    protected abstract String _update();
    protected abstract String _insert();
    protected abstract String _delete();

    public void save() throws SQLException, IOException, ClassNotFoundException {
        Connection conn = MyConnectionClass.getInstance();
        Statement s = conn.createStatement();
        if (_builtFromDB) {
            System.out.print("Executing this command : " + _update() + "\n");
            s.executeUpdate(_update());
        } else {
            System.out.println("Executing this command: " + _insert() + "\n");

            // Récuoérer la valeur de clé artificielle (auto_incrément)
            s.executeLargeUpdate(_insert(), Statement.RETURN_GENERATED_KEYS);
            _builtFromDB = true;
            ResultSet r = s.getGeneratedKeys();
            while (r.next()) {
                id = r.getInt(1);
            }
        }
    }
}
