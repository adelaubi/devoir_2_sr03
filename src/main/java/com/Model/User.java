package com.Model;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User extends ActiveRecordBase {

    private String lastName;
    private String firstName;
    private String login; //mail adress
    private String gender;
    private String pwd;
    private Role role = Role.Other;
    private static String _query = "select * from users"; // for findAll static Method
    private ArrayList<Forum> forumSubscriptions;
    public ArrayList<Forum> getForumSubscriptions() {
        return forumSubscriptions;
    }

    private enum Role {
        Other, Admin
    };

    public User() {
        _builtFromDB = false;
        this.forumSubscriptions = new ArrayList<Forum>();
    }

    public User(String lastName, String firstName, String login, String gender, String pwd) {

        this.lastName = lastName;
        this.firstName = firstName;
        this.login = login;
        this.gender = gender;
        this.pwd = pwd;
        this.forumSubscriptions = new ArrayList<Forum>();
        _builtFromDB = false;
    }

    public User(ResultSet res) throws SQLException, IOException, ClassNotFoundException { // PB
        this.id = res.getInt("id");
        this.firstName = res.getString("fname");
        this.lastName = res.getString("lname");
        this.login = res.getString("login");
        this.gender = res.getString("gender");
        this.role = Role.values()[res.getBoolean("is_admin") ? 1 : 0];
        this.forumSubscriptions = new ArrayList<Forum>();
        this.loadSubscription(); // PB3
        _builtFromDB = true;
    }

    public User(int id) throws IOException, ClassNotFoundException, SQLException {
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "select * from users where id = " + id + ";";
        Statement sql = null;
        sql = conn.createStatement();
        ResultSet res = sql.executeQuery(select_query);
        if (res.next()) {
            this.id = res.getInt("id");
            this.firstName = res.getString("fname");
            this.lastName = res.getString("lname");
            this.login = res.getString("login");
            this.gender = res.getString("gender");
            this.role = Role.values()[res.getBoolean("is_admin") ? 1 : 0];
            this.forumSubscriptions = new ArrayList<Forum>();
            _builtFromDB = true;
        }

    }

    public void setLogin(String login) {this.login = login;}
    public void setGender(String gender) {this.gender = gender;}
    public String getGender() {return gender;}
    public void setPwd(String pwd) {this.pwd = pwd;}
    public String getLogin() {return login; }
    public String getPwd() {return pwd;}
    public String getLastName() {return lastName;}
    public String getFirstName() {return firstName;}
    public void setLastName(String lastName) {this.lastName = lastName;}
    public void setFirstName(String firstName) {this.firstName = firstName;}
    public void setRole(String role) {this.role = Role.valueOf(role);}
    public String getRole() {return role.toString();}

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.lastName);
        hash = 97 * hash + Objects.hashCode(this.firstName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (obj == null) {return false;}
        if (getClass() != obj.getClass()) {return false;}
        final User other = (User) obj;
        if (!Objects.equals(this.lastName, other.lastName)) {return false;}
        return Objects.equals(this.firstName, other.firstName);
    }

    public User(String lastName, String firstName) {this.lastName = lastName; this.firstName = firstName;}

    @Override
    public String toString() {
        return "User{" + "lastName=" + lastName + ", firstName=" + firstName + ""
                + ", login=" + login + ", gender=" + gender + ","
                + " pwd=" + pwd + '}';
    }

    //methodes pour implémenter Active record
    @Override
    protected String _update() {
        return "UPDATE users SET fname = '" + firstName + "', `lname` = '" + lastName + "',"
                + " login = '" + login + "', is_admin = " + (role == Role.Admin ? "1" : "0") + ", gender = '" + gender + "', pwd = '" + pwd + "' WHERE id = " + id + ";";
    }

    @Override
    protected String _insert() {
        return "INSERT INTO users (fname, lname, login, gender, is_admin,pwd) "
                + "VALUES ('" + firstName + "', '" + lastName + "', '" + login + "', '" + gender + "', " + (role == Role.Admin ? "1" : "0") + ",'" + pwd + "');";
    }

    @Override
    protected String _delete() {
        return "DELETE FROM users WHERE (id = " + id + ");";
    }

    public static User FindByID(int id) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "SELECT * FROM users WHERE id =" + id + ";";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        ResultSet res = sql.executeQuery();
        if (res.next()){
            User u = new User(res);
            return u;
        }
        else return null;

    }

    public static User FindByloginAndPwd(String login, String pwd) throws IOException, ClassNotFoundException, SQLException {
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "select * from users where login = ? and pwd = ? ;";
        PreparedStatement sql = null;
        sql = conn.prepareStatement(select_query);
        sql.setString(1, login);
        sql.setString(2, pwd);
        ResultSet res = sql.executeQuery();
        if (res.next()) {
            User u = new User(res); // PB 2
            return u;
        }
        return null;
    }

    public static List<User> FindAll() throws IOException, ClassNotFoundException, SQLException {
        List <User>  listUser = new ArrayList<User>() ;

        Connection conn = MyConnectionClass.getInstance();
        Statement sql = conn.createStatement();
        ResultSet res = sql.executeQuery(_query);
        while (res.next()) {
            User newUser= new User (res);
            listUser.add(newUser);
        }

        return listUser;
    }

    public static User FindUserByLogin(String login) throws IOException, ClassNotFoundException,SQLException{
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "select * from users where login ='"+login+"' ;";
        PreparedStatement  sql= null;
        sql = conn.prepareStatement(select_query);
        ResultSet res = sql.executeQuery();
        if (res.next()){
            User u = new User(res);
            return u;
        }
        return null;
    }
    public static User FindByLastAndFirstName(String fname, String lname) throws IOException, ClassNotFoundException, SQLException {
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "select * from users where fname = ? and lname = ? ;";
        PreparedStatement sql = conn.prepareStatement(select_query);
        sql.setString(1, fname);
        sql.setString(2, lname);
        ResultSet res = sql.executeQuery();
        if (res.next()) {
            User u = new User(res);
            return u;
        }
        return null;
    }

    public void addForumSubscription(int id_forum) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = MyConnectionClass.getInstance();
        String add_query = "INSERT INTO subscription(id_forum,id_user) VALUES ("+id_forum+","+this.getId()+");";
        Statement s = null;
        s = conn.createStatement();
        s.executeUpdate(add_query);
        this.forumSubscriptions.add(Forum.findById(id_forum));
    }
    public void deleteForumSubscriptions(int id_forum) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = MyConnectionClass.getInstance();
        String delete_query = "DELETE FROM subscription WHERE id_forum = " + id_forum + " AND id_user = " + this.getId() ;
        Statement s = null;
        s = conn.createStatement();
        s.executeUpdate(delete_query);
        this.forumSubscriptions.remove(Forum.findById(id_forum));
    }

    public void loadSubscription() throws SQLException, IOException, ClassNotFoundException { // PB
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "SELECT * FROM subscription WHERE id_user = "  + this.getId();
        PreparedStatement  sql= conn.prepareStatement(select_query);
        ResultSet res = sql.executeQuery();
        while (res.next()){
            int forum_id = res.getInt("id_forum");
            this.forumSubscriptions.add(new Forum(forum_id));
        }
    }

    public List<Forum> findYourForum() throws SQLException, IOException, ClassNotFoundException {
        List<Forum> ownerForum = new ArrayList<Forum>();
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "SELECT * FROM forum WHERE owner = " + this.getId();
        PreparedStatement sql = conn.prepareStatement(select_query);
        ResultSet res = sql.executeQuery();
        while(res.next()){
            Forum newForum = new Forum(res);
            ownerForum.add(newForum);
        }
        return ownerForum;
    }

    public boolean isInForum(int idForum) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = MyConnectionClass.getInstance();
        String select_query = "SELECT * FROM subscription WHERE id_user = " + this.getId() + " AND id_forum = " + idForum;
        PreparedStatement sql = conn.prepareStatement(select_query);
        ResultSet res = sql.executeQuery();
        return res.next();
    }
}
