package com.WebSocket;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig;
import java.util.Hashtable;


@ServerEndpoint(value="/chatserver/{pseudo}/{idForum}",
        configurator=ChatServer.EndpointConfigurator.class)
public class ChatServer {

    private static ChatServer singleton = new ChatServer();
    /**
     * On maintient toutes les sessions utilisateurs dans une collection.
     */
    private Hashtable<String, Session> sessions = new Hashtable<>();


    private ChatServer() {}
    /**
     * Acquisition de notre unique instance ChatServer
     */
    public static ChatServer getInstance() {
        return ChatServer.singleton;
    }


    /**
     * Cette m�thode est d�clench�e � chaque connexion d'un utilisateur.
     */
    @OnOpen
    public void open(Session session, @PathParam("pseudo") String pseudo, @PathParam("idForum") String idForum ) {
        session.getUserProperties().put( "pseudo", pseudo );
        session.getUserProperties().put( "idForum", idForum );
        sessions.put(session.getId(),session);
        sendMessage("Admin >>> " + pseudo + ">>>a rejoint le salon.", idForum);
        String message = "Admin >>> Liste des participants |";
        for (Session s : sessions.values()){
            if(idForum.equals((String) s.getUserProperties().get("idForum")))
                message += s.getUserProperties().get("pseudo") + "|";
        }
        sendMessage(message, idForum);
    }

    /**
     * Cette m�thode est d�clench�e � chaque d�connexion d'un utilisateur.
     */
    @OnClose
    public void close(Session session, @PathParam("idForum") String idForum) {
        String pseudo = (String) session.getUserProperties().get( "pseudo" );
        sessions.remove(session.getId());
        sendMessage( "Admin >>>" + pseudo + ">>>a quitté le salon.", idForum);
          String message = "Admin >>> Liste des participants |";
        for (Session s : sessions.values()){
            if(idForum.equals((String) s.getUserProperties().get("idForum")))
                message += s.getUserProperties().get("pseudo") + "|";
        }
        sendMessage(message, idForum);
    }

    /**
     * Cette m�thode est d�clench�e en cas d'erreur de communication.
     */
    @OnError
    public void onError(Throwable error) {
        System.out.println( "Error: " + error.getMessage() );
    }

    /**
     * Cette m�thode est d�clench�e � chaque r�ception d'un message utilisateur.
     */
    @OnMessage
    public void handleMessage(String message, Session session, @PathParam("idForum") String idForum) {
        String pseudo = (String) session.getUserProperties().get( "pseudo" );
        String fullMessage = pseudo + " >>> " + message;
        sendMessage(fullMessage, idForum);
    }

    /**
     * Une m�thode priv�e, sp�cifique � notre exemple.
     * Elle permet l'envoie d'un message aux participants de la discussion.
     */
    private void sendMessage(String fullMessage, String idForum) {
        // Affichage sur la console du server Web.
        System.out.println( fullMessage );

        // On envoie le message � tout le monde.
        for( Session session : sessions.values()) {
            try {
                if(idForum.equals((String) session.getUserProperties().get("idForum"))) {
                    session.getBasicRemote().sendText(fullMessage);
                }
            } catch (Exception exception) {
                System.out.println("ERREUR: Message non envoyé à  " + session.getId());
            }
        }
    }

    /**
     * Permet de ne pas avoir une instance diff�rente par client.
     * ChatServer est donc g�rer en "singleton" et le configurateur utilise ce singleton.
     */
    public static class EndpointConfigurator extends ServerEndpointConfig.Configurator {
        @Override
        @SuppressWarnings("unchecked")
        public <T> T getEndpointInstance(Class<T> endpointClass) {
            return (T) ChatServer.getInstance();
        }
    }
}
