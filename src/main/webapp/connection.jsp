<%@ page import="com.HtmlClass.Navbar" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Connexion à votre Compte</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="titrePage">
Connexion à votre compte
</div>
<form class="formulaire" action="ServletConnection" method="POST">

  <table class="tableauAuthentification">
  <tr>
    <td><label for="username">Nom d'utilisateur :</label></td>
    <td><input type="text" id="username" name="username"></td>
  </tr>

  <tr>
    <td><label for="pass">Mot de passe :</label></td>
    <td><input type="password" id="pass" name="password"
           required></td>
  </tr>
  </table>
  <input type="submit" value="Sign in">

</form>
</body>
</html>
