<%@ page import="com.HtmlClass.Navbar" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ajouter un salon de tchatting</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
<%
HttpSession Session = request.getSession();
out.println(Navbar.getNavbar((String)Session.getAttribute("role")));
%>
<div class="titrePage">
  Ajout d'un salon de chat
</div>
<form class="formulaire" action="ServletForumManager" method="post">
  <table class="tableauAuthentification">
<tr>
  <td>
  <label>
    Nom du salon :</label></td>
    <td><input type="text" name="title"></td>

</tr>

      <tr>
          <td><label>
              Description :</label></td>
          <td><input type="text" name="description" id="description"></td>

      </tr>
      <tr>
      <td><label>
          Date d'expiration :</label></td>
      <td><input type="date" name="expiration" id="expiration" placeholder="yyyy-MM-jj"></td>

  </tr>
  </table>
  <input type="submit" value="Créer le salon">
</form>

</body>
</html>
