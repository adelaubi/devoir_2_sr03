window.addEventListener( "load", function( event ) {
    let pseudo = document.getElementById("pseudo").value;
    let idForum = document.getElementById("idForum").value;
    let ws = new WebSocket( "ws://localhost:8084/devoir_2_SR03_war_exploded/chatserver/" + pseudo + "/"+ idForum);
    let txtHistory = document.getElementById( "history" );
    let txtMessage = document.getElementById( "txtMessage" );
    let listeParticipants = document.getElementById("participants");
    txtMessage.focus();

    ws.addEventListener( "open", function( evt ) {
        //alert("Connection established");
    });

    ws.addEventListener( "message", function( evt ) {
        let message = evt.data;
        console.log( "Receive new message: " + message );
        /* Récupération du texte à afficher */
        let pseudoClient = String(pseudo);
        let date = new Date();
        let texteHeure = document.createTextNode(date.toLocaleTimeString());
        let textePseudo = document.createTextNode(getPseudo(message));
        let texteMessage = document.createTextNode(getMessage(message));

        if (textePseudo.textContent.substring(0,5) === "Admin") {
            let pseudoMouvement = getPseudo(getMessage(message));
            let MessageMouvement = getMessage(getMessage(message));
            if (getMessage(message).includes("Liste des participants")) {
                listeParticipants.innerHTML = "";
                let MessageParticipants = getMessage(message);
                let debut = false;
                let pseudonyme = "";
                for (let i = 0; i < MessageParticipants.length; i++) {
                    if (debut) {
                        if (MessageParticipants.charAt(i) === '|') {
                            let champPseudo = document.createElement("div");
                            champPseudo.classList.add("participant");
                            champPseudo.setAttribute("id", pseudonyme);
                            champPseudo.appendChild(document.createTextNode(pseudonyme));
                            listeParticipants.appendChild(champPseudo);
                            pseudonyme = "";
                        }
                        else {
                            pseudonyme += MessageParticipants.charAt(i);
                        }
                    }
                    else {
                        console.log(i);
                        if (MessageParticipants.charAt(i) == '|')
                            debut = true;
                    }

                }
            } else {
                texteMessage = document.createTextNode(pseudoMouvement + " " + MessageMouvement);
                let champEntree = document.createElement("div");
                champEntree.classList.add("messageEntree");
                let champHeure = document.createElement("span");
                champHeure.classList.add("heure");
                let champMessage = document.createElement("span");
                champMessage.classList.add("message");

                champHeure.appendChild(texteHeure);
                champMessage.appendChild(texteMessage);
                champEntree.appendChild(champHeure);
                champEntree.appendChild(champMessage);
                txtHistory.appendChild(champEntree);
            }
            /*
            if (MessageMouvement.includes("quitté")) {
                let champPseudo = document.getElementById(pseudoMouvement);
                listeParticipants.removeChild(champPseudo);
                alert('Ca va jusque là 3');
            }
            if (MessageMouvement.includes("rejoint")) {
                let champPseudo = document.createElement("div");
                champPseudo.classList.add("participant");
                champPseudo.setAttribute("id", pseudoMouvement);
                champPseudo.appendChild(document.createTextNode(pseudoMouvement));
                listeParticipants.appendChild(champPseudo);
            } */



        }
        else {
            /* Création des éléments à ajouter */

            let champmessage = document.createElement("div");
            let classeMessage = (pseudoClient.substring(0, pseudoClient.length - 1) === textePseudo.textContent.substring(0, pseudoClient.length - 1)
                && textePseudo.textContent.length === pseudoClient.length) ? "messageEnvoye" : "messageRecu";
            champmessage.classList.add(classeMessage);
            let contexte = document.createElement("div");
            contexte.classList.add("contexte");

            let heure = document.createElement("span");
            heure.classList.add("heure");
            let texte = document.createElement("span");
            texte.classList.add("message");
            let sender = document.createElement("span");
            sender.classList.add("pseudo");

            /* Ajout des éléments dans la page*/
            heure.appendChild(texteHeure);
            sender.appendChild(textePseudo);
            texte.appendChild(texteMessage);
            contexte.appendChild(heure);
            contexte.appendChild(sender);
            champmessage.appendChild(contexte);
            champmessage.appendChild(texte);
            txtHistory.appendChild(champmessage);
        }
    });

    ws.addEventListener( "close", function( evt ) {
        console.log( "Connection closed" );
    });


    let btnSend = document.getElementById( "btnSend" );
    btnSend.addEventListener( "click", function( clickEvent ) {
        ws.send( txtMessage.value );
        //alert('on envoie le message' + txtMessage.value);
        txtMessage.value = "";
        txtMessage.focus();
    });

    txtMessage.addEventListener("keypress", function (e){
        if (e.key === 'Enter'){
            ws.send( txtMessage.value );
            txtMessage.value = "";
            txtMessage.focus();
        }
    });

    let btnClose = document.getElementById( "btnClose" );
    btnClose.addEventListener( "click", function( clickEvent ) {
        ws.close();
        document.location.href="ServletMenu";
    });

});


function getPseudo(message) {
    let pseudo = "";
    for (let i = 0; i < message.length; i++)
    {
        if (message[i] == '>' && message[i+1] == '>' && message[i+2] == '>')
            return pseudo;
        else
            pseudo += message[i];
    }
}

function getMessage(message) {
    let debut = false;
    let result = "";
    for (var i = 0; i < message.length; i++) {
        if (message[i] == '>' && message[i + 1] == '>' && message[i + 2] == '>')
            debut = true;
        if (debut)
            result += message[i];
    }
    return result.substring(3);
}
