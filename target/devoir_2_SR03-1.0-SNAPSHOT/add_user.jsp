<%@ page import="com.HtmlClass.Navbar" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ajouter un utilisateur</title>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<%
    HttpSession Session = request.getSession();
    out.println(Navbar.getNavbar((String)Session.getAttribute("role")));
%>
<div class="titrePage">
    Ajout d'un utilisateur
</div>
<form class="formulaire" action="ServletUserManager" method="post">
    <table class="tableauAuthentification">
        <tr><td colspan="2"><span class="titre"> Informations générales </span></td></tr>
        <tr><td><label> First name </label></td><td><input class="inputLong" type="text" id="frname" name="frname" required/></td></tr>
        <tr><td><label> Familly name </label> </td><td><input class="inputLong" type="text" id="faname" name="faname" required/></td></tr>
        <tr><td><label> Email </label> </td><td><input class="inputLong" type="email" id="email" name="email" required/> </td></tr>
        <tr><td><label> Password </label> </td><td><input class="inputLong" type="password" id="pwd" name="pwd" required/></td></tr>
        <tr><td><label class="titre"> Sexe</label> </td><td>
    <label> Mâle </label> <input type="radio" id="male" name="gender" value="male" checked/><br/>
    <label> Femelle </label> <input type="radio" id="female" name="gender" value="female"/> </td></tr>
        <tr><td><label class="titre"> Role </label></td><td>
    <label> Admin </label> <input type="checkbox" name="role" value="Admin"></td></tr>
    </table>
    <input type="submit" value="Submit"> </form>
</body>
</html>
