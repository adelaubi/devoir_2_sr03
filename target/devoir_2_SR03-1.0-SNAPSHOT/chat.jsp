<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.Model.Forum"%>
<%@ page import="com.Model.User" %>
<%@ page import="java.sql.SQLException" %>
<html>
<head>
    <title>
    <%
        try {
            Forum forum = Forum.findById(Integer.parseInt(request.getParameter("forumId")));
            out.println("Forum : " + forum.getTitle());

        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    %>
    </title>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <script type="text/javascript" src="Client.js"></script>
</head>
<body>
    <div class="corpsChat" id="history"></div>
    <div class="listeParticipants" id="participants"></div>
    <input type="hidden" name="pseudo" id="pseudo" value="<%out.println(request.getSession().getAttribute("login"));%>"/>
    <input type="hidden" name="idForum" id="idForum" value="<%out.println(Integer.parseInt(request.getParameter("forumId")));%>"/>
    <input  class="inputCourt" id="txtMessage" type="text" />
    <button class="bouton" id="btnSend">Send message</button>
    <button class="bouton" id="btnClose">Close connection</button>
</body>
</html>
