<%@ page import="com.HtmlClass.Navbar" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="com.Model.Forum" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modifier un salon de tchatting</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<%
    HttpSession Session = request.getSession();
    out.println(Navbar.getNavbar((String)Session.getAttribute("role")));
    int forumId = Integer.parseInt(request.getParameter("id"));
    Forum forum = new Forum(forumId);
%>
<div class="titrePage">
    Modification d'un salon de chat
</div>
<form class="formulaire" action="ServletEditForum" method="post">
    <table class="tableauAuthentification">
        <tr>
            <td>
                <label>
                    Nom du salon :</label></td>
            <td><input type="text" name="title" value="<% out.print(forum.getTitle()); %>"></td>
        </tr>

        <input type="hidden" name="forumId" value="<%out.print(forumId);%>">
        <tr>
            <td><label>
                Description :</label></td>
            <td><input type="text" name="description" id="description" value="<% out.print(forum.getDescription()); %>"></td>

        </tr>
        <tr>
            <td><label>
                Date d'expiration :</label></td>
            <td><input type="date" name="expiration" id="expiration" placeholder="yyyy-MM-jj" value="<% out.print(forum.getExpirationToString()); %>"></td>

        </tr>
    </table>
    <input type="submit" value="Modifier le salon">
</form>

</body>
</html>
